﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MobWeb.Migrations
{
    public partial class PopularDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("INSERT INTO USUARIOS (Nome, Apelido, Email, Senha, Telefone) VALUES ('Kaleo', 'Eric', 'kaleo@kaleo.com', 'senhakaleo', '999999999')");
            migrationBuilder.Sql("INSERT INTO CLIENTES (RazaoSocial, CNPJ, Logradouro, Numero, Bairro, Complemento, CEP, UsuarioId) VALUES ('KaleoEmpresa', '11111111111111', 'Rua do Kaleo', 123, 'BairroK', 'Empresona', '12123123', 1)");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DELETE FROM USUARIOS");
            migrationBuilder.Sql("DELETE FROM CLIENTES");
        }
    }
}
