﻿using MobWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobWeb.Repositories
{
    public interface IClienteRepository
    {
        IEnumerable<Cliente> ListaClientes(int idUsuario);
        Cliente GetClienteById(int? clienteId);
        void Salvar(Cliente cliente);
        void Atualizar(Cliente cliente);
        void Deletar(Cliente clienteId);
    }
}
