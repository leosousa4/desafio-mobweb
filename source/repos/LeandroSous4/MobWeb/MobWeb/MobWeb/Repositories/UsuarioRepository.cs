﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MobWeb.Models;
using MobWeb.Context;
using Microsoft.AspNetCore.Http;

namespace MobWeb.Repositories
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly AppDbContext _context;

        public UsuarioRepository(AppDbContext contexto)
        {
            _context = contexto;
        }

        public void Salvar(Usuario usuario)
        {
            _context.Usuarios.Add(usuario);
            _context.SaveChanges();
        }
        public void Atualizar(Usuario usuario)
        {
            Usuario u1 = _context.Usuarios.FirstOrDefault(u => u.UsuarioId == usuario.UsuarioId);
            u1.Nome = usuario.Nome;
            u1.Apelido = usuario.Apelido;
            u1.Email = usuario.Email;
            u1.Senha = usuario.Senha;
            u1.Telefone = usuario.Telefone;
            _context.SaveChanges();
        }
        public Usuario GetUsuario(Usuario usuario)
        {
            return _context.Usuarios.FirstOrDefault(u => u.Email == usuario.Email && u.Senha == usuario.Senha);
        }
        public Usuario DadosUsuario(int id)
        {
            return _context.Usuarios.FirstOrDefault(u => u.UsuarioId == id);
        }
        public void Deletar(Usuario usuarioId)
        {
            _context.Usuarios.Remove(usuarioId);
            _context.SaveChanges();
        }
    }
}
