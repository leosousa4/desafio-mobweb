﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MobWeb.Models
{
    public class Cliente
    {
        public int ClienteId { get; set; }
        [StringLength(100)]
        [Required(ErrorMessage = "Digite sua Razão Social")]
        public string RazaoSocial { get; set; }
        [StringLength(14)]
        [Required(ErrorMessage = "Digite seu CNPJ")]
        public string CNPJ { get; set; }
        [StringLength(100)]
        [Required(ErrorMessage = "Digite seu Logradouro")]
        public string Logradouro { get; set; }
        [Required]
        public int Numero { get; set; }
        [StringLength(45)]
        [Required(ErrorMessage = "Digite seu Bairro")]
        public string Bairro { get; set; }
        [StringLength(45)]
        [Required(ErrorMessage = "Digite seu Complemento")]
        public string Complemento { get; set; }
        [StringLength(8)]
        [Required(ErrorMessage = "Digite seu CEP")]
        public string CEP { get; set; }
        public int UsuarioId { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
