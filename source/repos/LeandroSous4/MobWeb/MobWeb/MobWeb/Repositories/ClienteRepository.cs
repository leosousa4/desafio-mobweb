﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MobWeb.Models;
using MobWeb.Context;
using Microsoft.EntityFrameworkCore;

namespace MobWeb.Repositories
{
    public class ClienteRepository : IClienteRepository
    {
        private readonly AppDbContext _context;

        public ClienteRepository(AppDbContext contexto)
        {
            _context = contexto;
        }

        public IEnumerable<Cliente> ListaClientes(int idUsuario)
        {
            return _context.Clientes.ToList().Where(c => c.UsuarioId == idUsuario);
        }

        public void Salvar(Cliente cliente)
        {
            _context.Clientes.Add(cliente);
            _context.SaveChanges();
        }
        public void Atualizar(Cliente cliente)
        {
            Cliente c1 = _context.Clientes.FirstOrDefault(c => c.ClienteId == cliente.ClienteId);
            c1.RazaoSocial = cliente.RazaoSocial;
            c1.CNPJ = cliente.CNPJ;
            c1.Logradouro = cliente.Logradouro;
            c1.Numero = cliente.Numero;
            c1.Bairro = cliente.Bairro;
            c1.Complemento = cliente.Complemento;
            c1.CEP = cliente.CEP;
            _context.SaveChanges();
        }
        public Cliente GetClienteById(int? clienteId)
        {
            return _context.Clientes.FirstOrDefault(c => c.ClienteId == clienteId);
        }
        public void Deletar(Cliente clienteID)
        {
            _context.Clientes.Remove(clienteID);
            _context.SaveChanges();
        }
    }
}
