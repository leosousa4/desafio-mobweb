﻿using Microsoft.AspNetCore.Http;
using MobWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobWeb.Repositories
{
    public interface IUsuarioRepository
    {
        Usuario GetUsuario(Usuario usuario);
        Usuario DadosUsuario(int id);
        void Salvar(Usuario usuario);
        void Atualizar(Usuario usuario);
        void Deletar(Usuario usuario);
    }
}
