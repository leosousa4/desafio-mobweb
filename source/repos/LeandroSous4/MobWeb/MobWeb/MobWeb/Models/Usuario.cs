﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MobWeb.Models
{
    public class Usuario
    {
        public int UsuarioId { get; set; }
        [StringLength(45)]
        [Required(ErrorMessage = "Digite seu Nome")]
        public string Nome { get; set; }
        [StringLength(45)]
        [Required(ErrorMessage = "Digite seu Apelido")]
        public string Apelido { get; set; }
        [StringLength(45)]
        [Required(ErrorMessage = "Digite seu E-mail")]
        public string Email { get; set; }
        [StringLength(16)]
        [Required(ErrorMessage = "Digite sua Senha")]
        public string Senha { get; set; }
        [StringLength(11)]
        [Required(ErrorMessage = "Digite seu Telefone")]
        public string Telefone { get; set; }
        public List<Cliente> Clientes { get; set; }
        IHttpContextAccessor HttpContextAccessor { get; set; }

        public Usuario() { }

        public Usuario(IHttpContextAccessor httpContextAccessor)
        {
            HttpContextAccessor = httpContextAccessor;
        }
    }
}
