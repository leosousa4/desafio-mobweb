﻿using Microsoft.EntityFrameworkCore;
using MobWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobWeb.Context
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        { }

        public DbSet<Usuario> Usuarios{ get; set; }
        public DbSet<Cliente> Clientes { get; set; }
    }
}
