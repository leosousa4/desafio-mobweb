﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MobWeb.Repositories;
using MobWeb.Models;
using Microsoft.AspNetCore.Http;

namespace MobWeb.Controllers
{
    public class ClienteController : Controller
    {
        IHttpContextAccessor HttpContextAccessor;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IClienteRepository _clienteRepository;

        public ClienteController(IHttpContextAccessor httpContextAccessor, IUsuarioRepository usuarioRepository, IClienteRepository clienteRepository)
        {
            HttpContextAccessor = httpContextAccessor;
            _usuarioRepository = usuarioRepository;
            _clienteRepository = clienteRepository;
        }
        
        public IActionResult List()
        {
            try
            {
                var idUsuario = int.Parse(HttpContextAccessor.HttpContext.Session.GetString("IdUsuarioLogado"));
                ViewBag.ListaClientes = _clienteRepository.ListaClientes(idUsuario);
                return View();
            }
            catch
            {
                return RedirectToAction("Login", "Usuario");
            }
        }
        [HttpPost]
        public IActionResult CadastrarCliente(Cliente cliente)
        {
            if(cliente.ClienteId == 0)
            {
                if (ModelState.IsValid)
                {
                    cliente.UsuarioId = int.Parse(HttpContextAccessor.HttpContext.Session.GetString("IdUsuarioLogado"));
                    _clienteRepository.Salvar(cliente);
                }
                else
                {
                    return View();
                }
                return RedirectToAction("List");
                
            }
            else
            {
                _clienteRepository.Atualizar(cliente);
                return RedirectToAction("List");
            }
        }
        [HttpGet]
        public IActionResult CadastrarCliente(int? id)
        {
            if (id != 0)
            {
                ViewBag.Cliente = _clienteRepository.GetClienteById(id);
            }
            return View();
        }

        [HttpPost]
        [HttpGet]
        public IActionResult DeletarCliente(int id)
        {
            Cliente cliente = _clienteRepository.GetClienteById(id);
            _clienteRepository.Deletar(cliente);
            return RedirectToAction("List");
        }
    }
}