﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MobWeb.Repositories;
using Microsoft.AspNetCore.Http;
using MobWeb.Models;

namespace MobWeb.Controllers
{
    public class UsuarioController : Controller
    {
        IHttpContextAccessor HttpContextAccessor;
        private readonly IUsuarioRepository _usuarioRepository;
        private readonly IClienteRepository _clienteRepository;

        public UsuarioController(IHttpContextAccessor httpContextAccessor, IUsuarioRepository usuarioRepository, IClienteRepository clienteRepository)
        {
            HttpContextAccessor = httpContextAccessor;
            _usuarioRepository = usuarioRepository;
            _clienteRepository = clienteRepository;
        }


        [HttpGet]
        public IActionResult Login(int? id)
        {
            if (id != null)
            {
                if (id == 0)
                {
                    HttpContext.Session.SetString("NomeUsuarioLogado", string.Empty);
                    HttpContext.Session.SetString("IdUsuarioLogado", string.Empty);
                }
            }
            return View();
        }
        [HttpPost]
        public IActionResult ValidarLogin(Usuario usuario)
        {
            Usuario Login = _usuarioRepository.GetUsuario(usuario);
            if (Login != null)
            {
                HttpContext.Session.SetString("NomeUsuarioLogado", Login.Nome);
                HttpContext.Session.SetString("IdUsuarioLogado", Login.UsuarioId.ToString());
                return RedirectToAction("List", "Cliente");
            }
            else
            {
                TempData["LoginInvalido"] = "Dados de login inválidos!";
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public IActionResult Registrar(Usuario usuario)
        {
            if(usuario.UsuarioId == 0)
            {
                if (ModelState.IsValid)
                {
                    _usuarioRepository.Salvar(usuario);
                    return RedirectToAction("Sucesso");
                }
            }
            else
            {
                _usuarioRepository.Atualizar(usuario);
                return RedirectToAction("MeusDados");

            }
            return View();
        }
        [HttpGet]
        public IActionResult Registrar()
        {
            return View();
        }
        public IActionResult MeusDados()
        {
            try
            {
                var idUsuario = int.Parse(HttpContextAccessor.HttpContext.Session.GetString("IdUsuarioLogado"));
                ViewBag.Usuario = _usuarioRepository.DadosUsuario(idUsuario);
                return View();
            }
            catch
            {
                return RedirectToAction("List", "Cliente");
            }
        }

        public IActionResult Sucesso()
        {
            return View();
        }
        [HttpGet]
        [HttpPost]
        public IActionResult DeletarConta()
        {
            var idUsuario = int.Parse(HttpContextAccessor.HttpContext.Session.GetString("IdUsuarioLogado"));
            Usuario usuario = _usuarioRepository.DadosUsuario(idUsuario);
            _usuarioRepository.Deletar(usuario);
            return RedirectToAction("Login");
        }
    }
}